import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { EmpdetailComponent } from './empdetail/empdetail.component';
import { EmpaddComponent } from './empadd/empadd.component';
import {FormsModule} from '@angular/forms';
import {Router,Routes,RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http'
const rootPatch :Routes = [{
  path :"",component:HomeComponent,

},
{
  path:"empAdd",component:EmpaddComponent
},
{
path:"empDetail",component:EmpdetailComponent
}]
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EmpdetailComponent,
    EmpaddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(rootPatch),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
