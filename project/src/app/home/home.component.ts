import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import {EmployeeItem } from '../Model/EmployeeItem'
import {Router,ActivatedRoute} from '@angular/router';
import {ApiService} from '../api/api.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  amount:any = 0;
   dataItem :any;
  constructor(private routepage:Router,private activepage:ActivatedRoute,private api:ApiService) { }

  ngOnInit() {
    this.api.GetPOST("EmpList",{
      "EmpName":""
    },(data:{})=>{
      this.dataItem = data;
      for(let i of this.dataItem){
        this.amount =this.amount +  i["empSalary"];
      }
      this.amount = this.amount.toFixed(2);
    })

  }
  getdetail(item:any){

    this.routepage.navigate(["/empDetail",{
      id:item.id
    }]);
  }


}
