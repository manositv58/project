import { HttpClient } from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
const URLAPI:string="http://localhost:32242/api/";
@Injectable({
  providedIn: 'root'
})
export class ApiService {

   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };
  constructor(private api:HttpClient) { }

  GetPOST(url,dataItem:any,functionCall:any){
    this.api.post<any>(URLAPI+url,dataItem,{
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    }).subscribe((data:{})=>{
      functionCall(data);
    })
  }

  GetGet(url,functionCall:any){
   this.api.get<any>(URLAPI+url).subscribe((data:{})=>{
    functionCall(data);
   });
  }



}
