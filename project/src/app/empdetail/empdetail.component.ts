import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api/api.service'
import {ActivatedRoute,ActivationEnd,Router,Routes} from '@angular/router'
import {EmployeeItem}  from '../Model/EmployeeItem';
import { modelGroupProvider } from '@angular/forms/src/directives/ng_model_group';
@Component({
  selector: 'app-empdetail',
  templateUrl: './empdetail.component.html',
  styleUrls: ['./empdetail.component.css']
})
export class EmpdetailComponent implements OnInit {
  empData :any;
  idList:Array<number> = [];
  constructor(private callAPI:ApiService,private rpage :Router,private ractive:ActivatedRoute) { }
  nextxpage(){
    if(this.empData.length>1){
     window.location.href="empDetail;id="+this.empData[1].id;
    }
  }

  ngOnInit() {


    var getdetailID = this.ractive.snapshot.params["id"];
    this.callAPI.GetPOST("EmpDetail",{
      "id":getdetailID
    },(data:{})=>{
  this.empData = data;
    });
  }



}
