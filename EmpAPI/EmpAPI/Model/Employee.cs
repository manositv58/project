﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EmpAPI.Model
{
    public class Employee
    {
        public string EmpName { get; set; }
        public string EmpSurName { get; set; }
        public string EmpDate { get; set; }
        public string EmpEmail { get; set; }
        public int id { get; set; }
        public float EmpSalary { get; set; }
        public Employee()
        {
            this.EmpDate = DateTime.Now.ToString("dd//MM/yyyy");
            this.EmpName = "";
            this.EmpEmail = "";
            this.EmpSurName = "";
            this.EmpSalary = 0;
        }
        
    }

    public class ConfigServer
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
