﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace EmpAPI.Database
{
    public class DatabaseConnect
    {
        private SqlConnection cn = null;
        private SqlDataAdapter da = null;
        private DataTable dt = null;
       private Model.ConfigServer config = null;
        public DatabaseConnect()
        {
            string message = System.IO.File.ReadAllText("config.json");
            config = Newtonsoft.Json.JsonConvert.DeserializeObject<Model.ConfigServer>(message);
            if (config.Username == "" && config.Password == "")
            {
                cn = new SqlConnection("data source=" + config.Server + "; database=" + config.Database + "; integrated security=true;");

            }
            else {
                cn = new SqlConnection("data source=" + config.Server + "; database=" + config.Database + "; user id=" + config.Username + "; password=" + config.Password + ";");
            }
        }
        public DataTable Query(string sql)
        {
            da = new SqlDataAdapter(sql,cn);
            dt = new DataTable();
            da.Fill(dt);
            return dt; 
        }

        public void Execute(string sql)
        {
            SqlCommand cm = new SqlCommand(sql,cn);
            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            }
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }
    }
}
