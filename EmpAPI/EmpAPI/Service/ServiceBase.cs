﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EmpAPI.Service
{
    public class ServiceBase  :ControllerBase
    {
        Database.DatabaseConnect db = null;
        public ServiceBase()
        {
            this.db = new Database.DatabaseConnect();
        }
        public System.Data.DataTable Save(Model.Employee input)
        {
            db.Execute("insert into Employee(EMPName,EmpSurName,EmpEmail,EmpDate,EmpSalary) values('" + input.EmpName + "','" + input.EmpSurName + "','" + input.EmpEmail + "','" + input.EmpDate + "','" + input.EmpSalary + "')");
            System.Data.DataTable dt = db.Query("select * from Employee");
            dt.Columns.Add("getDateItem");
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dt.Rows[i]["getDateItem"] = Convert.ToDateTime(dt.Rows[i]["EmpDate"]).ToString("dd/MM/yyyy");
            }
            return dt;
        }


        public System.Data.DataTable Edit(Model.Employee input)
        {
            db.Execute("update Employee set EmpName='"+input.EmpName+"',EmpSurName='"+input.EmpSurName+"',EmpDate='"+input.EmpDate+"',EmpEmail='"+input.EmpEmail+"',EmpSalary='"+input.EmpSalary+"' where id='"+input.id+"'");
            System.Data.DataTable dt = db.Query("select * from Employee where id='"+input.id+"'");
            dt.Columns.Add("getDateItem");
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dt.Rows[i]["getDateItem"] = Convert.ToDateTime(dt.Rows[i]["EmpDate"]).ToString("dd/MM/yyyy");
            }
            return dt;
        }

        public System.Data.DataTable Delete(Model.Employee input)
        {
            db.Execute("delete Employee where id='" + input.id + "'");
            System.Data.DataTable dt = db.Query("select * from Employee");
            dt.Columns.Add("getDateItem");
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dt.Rows[i]["getDateItem"] = Convert.ToDateTime(dt.Rows[i]["EmpDate"]).ToString("dd/MM/yyyy");
            }
            return dt;
        }

        public System.Data.DataTable GetItem(Model.Employee input)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = db.Query("select * from Employee where (EmpName like '%" + input.EmpName + "' or EmpSurName like '%" + input.EmpName + "%' or EmpEmail like '%" + input.EmpName + "%') or '" + input.EmpName + "'=''");
            dt.Columns.Add("getDateItem");
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                dt.Rows[i]["getDateItem"] = Convert.ToDateTime(dt.Rows[i]["EmpDate"]).ToString("dd/MM/yyyy");
            }
            return dt;
        }

        public System.Data.DataTable GetDetail(int id)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = db.Query("select * from Employee where id>='" + id + "'");
            dt.Columns.Add("getDateItem");
            for(int i=0; i<=dt.Rows.Count-1; i++)
            {
                dt.Rows[i]["getDateItem"] = Convert.ToDateTime(dt.Rows[i]["EmpDate"]).ToString("dd/MM/yyyy");
            }
            return dt;
        }
    }
}
